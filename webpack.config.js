var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    debug: true,
    entry: 'app',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel', exclude: /node_modules/},
            {test: /\.scss$/, loader: 'style!css!resolve-url!sass?sourceMap'},
            {test: /\.(woff|woff2|ttf|eot|svg)(\?]?.*)?$/, loader: 'url?limit=1024&name=fonts/[name].[ext]?[hash]'},
            {test: /\.jpe?g|gif$/i,loader: 'url?limit=8192&name=imgs/[name].[ext]?[hash]', exclude: /node_modules/},
            {test: /\.png$/i, loader: 'url?limit=8192&mimetype=image/png&name=imgs/[name].[ext]?[hash]', exclude: /node_modules/},
            {test: /\.css$/, loader: 'style!css!resolve-url', exclude: /node_modules/},
            {test: /\.html$/, loader: 'html?root=' + __dirname + '/www', exclude: /node_modules/}
        ]
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', 'html'],
        modulesDirectories: [
            'node_modules',
            path.resolve(__dirname, 'www')
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            pkg: require('./package.json'),
            template: './www/entry-template.html',
            inject: 'body'
        })
    ]
};
